//
//  NSString+Validation.m
//  ZoneTickets
//
//  Created by Nivendru on 30/04/15.
//  Copyright (c) 2015 NivendruGavaskar. All rights reserved.
//

#import "NSString+Validation.h"

@implementation NSString (Validation)
+(BOOL)validation:(NSString *)sendstring
{
    NSString *trimmed=sendstring;
    trimmed=[trimmed stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    if([trimmed length]==0)
    {
        return NO;
    }
    else
    {
        return YES;
    }
}
+ (BOOL) validateEmail:(NSString *) emailid
{
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:emailid];
}

+(BOOL)validation1 : (NSString *)sendstring1 : (NSRange)range :(NSString *)string
{
    NSCharacterSet *myCharSet;
    myCharSet = [NSCharacterSet characterSetWithCharactersInString:@"0123456789.-"];
    if([sendstring1 rangeOfString:@"-"].location!= NSNotFound&&[string isEqualToString:@"-"])
    {
        return NO;
    }
    else if([sendstring1 rangeOfString:@"."].location!= NSNotFound&&[string isEqualToString:@"."])
    {
        return NO;
    }
    
    for (int i = 0; i < [string length]; i++)
    {
        unichar c = [string characterAtIndex:i];
        if (![myCharSet characterIsMember:c])
        {
            return NO;
        }
    }
    
    return YES;
}

+(BOOL)validation2 : (NSString *)sendstring2 : (NSRange)range1 :(NSString *)string1
{
    NSCharacterSet *myCharSet;
    myCharSet = [NSCharacterSet characterSetWithCharactersInString:@"0123456789-"];
    if((![sendstring2 isEqualToString:@""])&&[string1 isEqualToString:@"-"])
    {
        return NO;
    }
    
    
    if([sendstring2 rangeOfString:@"-"].location!= NSNotFound&&[string1 isEqualToString:@"-"])
    {
        return NO;
    }
    
    for (int i = 0; i < [string1 length]; i++)
    {
        unichar c = [string1 characterAtIndex:i];
        if (![myCharSet characterIsMember:c])
        {
            return NO;
        }
    }
    
    return YES;
}

+(BOOL)validation3 : (NSString *)sendstring2 : (NSRange)range1 :(NSString *)string1
{
    NSCharacterSet *myCharSet;
    myCharSet = [NSCharacterSet characterSetWithCharactersInString:@"0123456789"];
    for (int i = 0; i < [string1 length]; i++)
    {
        unichar c = [string1 characterAtIndex:i];
        if (![myCharSet characterIsMember:c])
        {
            return NO;
        }
    }
    
    return YES;
}
@end
