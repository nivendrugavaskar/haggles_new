//
//  NSString+Validation.h
//  ZoneTickets
//
//  Created by Nivendru on 30/04/15.
//  Copyright (c) 2015 NivendruGavaskar. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (Validation)
+(BOOL)validation:(NSString *)sendstring;
+ (BOOL) validateEmail:(NSString *) emailid;

+(BOOL)validation1 : (NSString *)sendstring1 : (NSRange)range :(NSString *)string;
+(BOOL)validation2 : (NSString *)sendstring2 : (NSRange)range1 :(NSString *)string1;
+(BOOL)validation3 : (NSString *)sendstring2 : (NSRange)range1 :(NSString *)string1;

@end
