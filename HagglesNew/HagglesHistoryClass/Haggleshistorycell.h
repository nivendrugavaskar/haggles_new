//
//  Haggleshistorycell.h
//  HagglesNew
//
//  Created by Nivendru Gavaskar on 25/01/16.
//  Copyright © 2016 Nivendru Gavaskar. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Haggleshistorycell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIView *myvw;
@property (weak, nonatomic) IBOutlet UIImageView *imgvw;
@property (weak, nonatomic) IBOutlet UILabel *lbltitle;

@end
