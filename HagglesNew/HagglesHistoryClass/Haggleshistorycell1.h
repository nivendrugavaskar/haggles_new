//
//  Haggleshistorycell1.h
//  HagglesNew
//
//  Created by Nivendru Gavaskar on 25/01/16.
//  Copyright © 2016 Nivendru Gavaskar. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Haggleshistorycell1 : UITableViewCell

@property (weak, nonatomic) IBOutlet UIView *myvw;
@property (weak, nonatomic) IBOutlet UIButton *btnsuccess;
@property (weak, nonatomic) IBOutlet UIButton *btnunsuccessful;
@property (weak, nonatomic) IBOutlet UIButton *btnratehaggle;
@end
