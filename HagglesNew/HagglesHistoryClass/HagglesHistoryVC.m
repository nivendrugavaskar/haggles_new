//
//  HagglesHistoryVC.m
//  HagglesNew
//
//  Created by Nivendru Gavaskar on 25/01/16.
//  Copyright © 2016 Nivendru Gavaskar. All rights reserved.
//

#import "HagglesHistoryVC.h"
#import "Haggleshistorycell.h"
#import "Haggleshistorycell1.h"

@interface HagglesHistoryVC ()<UITableViewDataSource,UITableViewDelegate>

@end

@implementation HagglesHistoryVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)didTapBack:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma tableview data source and delegate
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(tableView.tag==1)
    {
    Haggleshistorycell1 *cell=(Haggleshistorycell1 *)[self.tblvw1 dequeueReusableCellWithIdentifier:@"Cell1" forIndexPath:indexPath];
    cell.myvw.layer.cornerRadius=5.0;
    cell.myvw.layer.borderColor=[UIColor lightGrayColor].CGColor;
    cell.myvw.layer.borderWidth=1.0;
        
        
        
    return cell;
    }
    else
    {
    Haggleshistorycell *cell=(Haggleshistorycell *)[self.tblvw dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    cell.myvw.layer.cornerRadius=5.0;
    cell.myvw.layer.borderColor=[UIColor lightGrayColor].CGColor;
    cell.myvw.layer.borderWidth=1.0;
    return cell;
    }
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(tableView.tag==1)
    {
        return 3;
    }
    return 5;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
