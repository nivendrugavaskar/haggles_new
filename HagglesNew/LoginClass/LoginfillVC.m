//
//  LoginfillVC.m
//  HagglesNew
//
//  Created by Nivendru Gavaskar on 21/01/16.
//  Copyright © 2016 Nivendru Gavaskar. All rights reserved.
//

#import "LoginfillVC.h"
#import "AppDelegate.h"

@interface LoginfillVC ()<UITextFieldDelegate>
{
    AppDelegate *app;
    NSDictionary *dict;
}

@end

@implementation LoginfillVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    app=(AppDelegate *)[UIApplication sharedApplication].delegate;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)didtapLogin:(id)sender {
    
    if(![NSString validation:_txtemail.text]||![NSString validation:_txtpwd.text])
    {
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:nil message:@"All fields are mandatory" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        return;
    }
    [self Loginservice];
    
}
- (IBAction)didtapBack:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)didtapSignUP:(id)sender {
    [self performSegueWithIdentifier:@"pushtosignup" sender:self];
}
- (IBAction)didTapForgetPWD:(id)sender {
    [self performSegueWithIdentifier:@"pushtoforgetpwd" sender:self];
}
#pragma textfield deelgate
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

-(void)Loginservice
{
    //
    //http:
    //208.74.19.152:8085/zoneswebservices/ws/GeneralizedSearch.json?configId=xxxx&customerId=&generalizedSearch=kinky&pageNumber=10
    
    NSString *path =[NSString stringWithFormat:@"%@Login",app->parentUrl];
    NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:_txtemail.text,@"UserLogin[username]",_txtpwd.text,@"UserLogin[password]",nil];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    // manager.responseSerializer.acceptableContentTypes = [manager.responseSerializer.acceptableContentTypes setByAddingObject:@"text/html"];
    // [[UIApplication sharedApplication] beginIgnoringInteractionEvents];
    [SVProgressHUD showWithStatus:@"Please wait..."];
    [manager POST:path parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         //  NSString *responseStr = [[NSString alloc] initWithData:operation.responseData encoding:NSUTF8StringEncoding];
         //NSLog(@"%@",responseStr);
         dict = [NSJSONSerialization JSONObjectWithData:operation.responseData options:NSJSONReadingAllowFragments error:NULL];
         if([[dict valueForKeyPath:@"status.status"] integerValue]==0)
         {
             UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"" message:[dict valueForKeyPath:@"status.error"] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
             [alert show];
         }
         else
         {
             app->loginstatus=TRUE;
             [self performSegueWithIdentifier:@"pushtotabbar" sender:self];
         }
         [SVProgressHUD dismiss];
     }
          failure:^(AFHTTPRequestOperation *operation, NSError *error)
     {
         // [[UIApplication sharedApplication] endIgnoringInteractionEvents];
         [SVProgressHUD dismiss];
         
     }];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
