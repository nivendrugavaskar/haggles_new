//
//  FirstViewController.m
//  HagglesNew
//
//  Created by Nivendru Gavaskar on 21/01/16.
//  Copyright © 2016 Nivendru Gavaskar. All rights reserved.
//

#import "HomeVC.h"
#import "HomeCell.h"
@interface HomeVC ()

@end

@implementation HomeVC

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
#pragma custom cell
    HomeCell *cell=(HomeCell *)[self.tblVw dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    cell.myvw.layer.borderColor=[UIColor lightGrayColor].CGColor;
    cell.myvw.layer.borderWidth=1.0;
    cell.myvw.layer.cornerRadius=5.0;
    
    
    
    return cell;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 5;
}
- (IBAction)didTapProductBuy:(id)sender {
    [self performSegueWithIdentifier:@"pushtoproductlist" sender:self];
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    [self performSegueWithIdentifier:@"pushtotrendinghaggle" sender:indexPath];
}

@end
