//
//  FirstViewController.h
//  HagglesNew
//
//  Created by Nivendru Gavaskar on 21/01/16.
//  Copyright © 2016 Nivendru Gavaskar. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HomeVC : UIViewController<UITableViewDataSource,UITableViewDelegate>
@property (weak, nonatomic) IBOutlet UITableView *tblVw;


@end

