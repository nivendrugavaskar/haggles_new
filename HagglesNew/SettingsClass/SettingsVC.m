//
//  SettingsVC.m
//  HagglesNew
//
//  Created by Nivendru Gavaskar on 21/01/16.
//  Copyright © 2016 Nivendru Gavaskar. All rights reserved.
//

#import "SettingsVC.h"
#import "AppDelegate.h"
#import "LoginVC.h"

@interface SettingsVC ()
{
    AppDelegate *app;
}

@end

@implementation SettingsVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    app=(AppDelegate *)[UIApplication sharedApplication].delegate;
    if(app->loginstatus)
    {
        _logged_view.hidden=FALSE;
        _loggedout_view.hidden=YES;
    }
    else
    {
        _logged_view.hidden=TRUE;
        _loggedout_view.hidden=FALSE;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)didTapLogout:(id)sender {
    app->loginstatus=FALSE;
    //[self performSegueWithIdentifier:@"pushtologin" sender:self];
    NSLog(@"%lu",self.navigationController.navigationController.viewControllers.count);
    
//     NSLog(@"%@",self.navigationController.navigationController.viewControllers[0]);
//     NSLog(@"%@",self.navigationController.navigationController.viewControllers[1]);
//     NSLog(@"%@",self.navigationController.navigationController.viewControllers[2]);
    
   [self.navigationController.navigationController popToRootViewControllerAnimated:YES];
}
- (IBAction)didtapSignUp:(id)sender
{
    [self.navigationController.navigationController popToRootViewControllerAnimated:YES];
    
    //[self performSegueWithIdentifier:@"pushtosignup" sender:self];
    
    
}
- (IBAction)didTapLogin:(id)sender
{
    [self.navigationController.navigationController popToRootViewControllerAnimated:YES];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)didtaphistory:(id)sender {
    [self performSegueWithIdentifier:@"pushtohaggleshistory" sender:self];
}

- (IBAction)didtapProfile:(id)sender {
}
@end
