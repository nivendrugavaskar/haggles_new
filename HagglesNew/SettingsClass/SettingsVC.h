//
//  SettingsVC.h
//  HagglesNew
//
//  Created by Nivendru Gavaskar on 21/01/16.
//  Copyright © 2016 Nivendru Gavaskar. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SettingsVC : UIViewController
@property (weak, nonatomic) IBOutlet UIView *logged_view;
@property (weak, nonatomic) IBOutlet UIView *loggedout_view;
- (IBAction)didtaphistory:(id)sender;
- (IBAction)didtapProfile:(id)sender;

@end
