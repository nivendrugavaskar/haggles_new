//
//  Myhagglescell.h
//  HagglesNew
//
//  Created by Nivendru Gavaskar on 22/01/16.
//  Copyright © 2016 Nivendru Gavaskar. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Myhagglescell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIView *myvw;
@property (weak, nonatomic) IBOutlet UIImageView *imgvw;
@property (weak, nonatomic) IBOutlet UILabel *lbltitle;

@end
