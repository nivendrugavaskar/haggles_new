//
//  MyHaggleVC.m
//  HagglesNew
//
//  Created by Nivendru Gavaskar on 21/01/16.
//  Copyright © 2016 Nivendru Gavaskar. All rights reserved.
//

#import "MyHaggleVC.h"
#import "Myhagglescell.h"

@interface MyHaggleVC ()<UITableViewDataSource,UITableViewDataSource>

@end

@implementation MyHaggleVC

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
#pragma custom cell
    Myhagglescell *cell=(Myhagglescell *)[self.tblVw dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    cell.myvw.layer.borderColor=[UIColor lightGrayColor].CGColor;
    cell.myvw.layer.borderWidth=1.0;
    cell.myvw.layer.cornerRadius=5.0;
    
    
    
    return cell;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 5;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
