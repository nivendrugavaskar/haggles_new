//
//  CustomTextfield.m
//  HagglesNew
//
//  Created by Nivendru Gavaskar on 21/01/16.
//  Copyright © 2016 Nivendru Gavaskar. All rights reserved.
//

#import "CustomTextfield.h"

@implementation CustomTextfield

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/
- (CGRect)textRectForBounds:(CGRect)bounds {
    return CGRectMake(bounds.origin.x + 10, bounds.origin.y + 8,
                      bounds.size.width - 20, bounds.size.height - 16);
}
- (CGRect)editingRectForBounds:(CGRect)bounds {
    return [self textRectForBounds:bounds];
}
@end
