//
//  ProductListVC.m
//  HagglesNew
//
//  Created by Nivendru Gavaskar on 21/01/16.
//  Copyright © 2016 Nivendru Gavaskar. All rights reserved.
//

#import "ProductListVC.h"
#import "AppDelegate.h"
#import "ProductlistCollectionCell.h"


static NSString * const CellIdentifier = @"Cell";

@interface ProductListVC ()
{
    AppDelegate *app;
    
    NSMutableArray *productnameArray,*productImageArray;
}

@end

@implementation ProductListVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    productnameArray=[[NSMutableArray alloc] initWithObjects:@"TV",@"Fridge",@"Sound System",@"Home Theatre",@"Camera",@"Washer",@"Aircon",@"BBQ",@"Projector", nil];
    productImageArray=[[NSMutableArray alloc] initWithObjects:@"tvicon2.png",@"fridge.png",@"soundsystem.png",@"hometheater.png",@"camera.png",@"washer.png",@"aircon.png",@"bbq.png",@"projector.png", nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)didTapBack:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
        float width;
        width=(self.collectionVW.frame.size.width/3)-10;
        return CGSizeMake(width, width);
    
    
}
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return productnameArray.count;
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView
                  cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    
        ProductlistCollectionCell *cell =(ProductlistCollectionCell *)[self.collectionVW dequeueReusableCellWithReuseIdentifier:CellIdentifier forIndexPath:indexPath];
    
    cell.myvw.layer.borderWidth=1.0;
    cell.myvw.layer.borderColor=[UIColor lightGrayColor].CGColor;
    cell.myvw.layer.cornerRadius=5.0;
    
        cell.lbtname.text=[productnameArray objectAtIndex:indexPath.row];
        cell.imgvw.image=[UIImage imageNamed:[productImageArray objectAtIndex:indexPath.row]];
        return cell;
}
-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    [collectionView deselectItemAtIndexPath:indexPath animated:YES];
    [self performSegueWithIdentifier:@"pushtoproductdetails" sender:indexPath];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
