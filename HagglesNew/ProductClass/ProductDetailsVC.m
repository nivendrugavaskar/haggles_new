//
//  ProductDetailsVC.m
//  HagglesNew
//
//  Created by Nivendru Gavaskar on 22/01/16.
//  Copyright © 2016 Nivendru Gavaskar. All rights reserved.
//

#import "ProductDetailsVC.h"

@interface ProductDetailsVC ()<UITextFieldDelegate>
{
    CGPoint pointtrack;
}

@end

@implementation ProductDetailsVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)didTapback:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    self.activeTextField=textField;
    pointtrack=_ascrollvw.contentOffset;
    [self.ascrollvw setContentOffset:CGPointMake(0, pointtrack.y+20)];
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
