//
//  ProductDetailsVC.h
//  HagglesNew
//
//  Created by Nivendru Gavaskar on 22/01/16.
//  Copyright © 2016 Nivendru Gavaskar. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ProductDetailsVC : UIViewController
@property (weak, nonatomic) IBOutlet UITextField *txtmodel;
@property (weak, nonatomic) IBOutlet UITextField *txtbrand;
@property (weak, nonatomic) IBOutlet UITextField *txttype;
@property (weak, nonatomic) IBOutlet UITextField *txtsize;
@property (weak, nonatomic) IBOutlet UITextField *txtprice;
@property (weak, nonatomic) IBOutlet UIScrollView *ascrollvw;


@property(weak,nonatomic)UITextField *activeTextField;
@end
