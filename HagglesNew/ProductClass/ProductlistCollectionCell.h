//
//  ProductlistCollectionCell.h
//  HagglesNew
//
//  Created by Nivendru Gavaskar on 21/01/16.
//  Copyright © 2016 Nivendru Gavaskar. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ProductlistCollectionCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imgvw;
@property (weak, nonatomic) IBOutlet UILabel *lbtname;
@property (weak, nonatomic) IBOutlet UIView *myvw;

@end
