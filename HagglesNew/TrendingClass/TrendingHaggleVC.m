//
//  TrendingHaggleVC.m
//  HagglesNew
//
//  Created by Nivendru Gavaskar on 22/01/16.
//  Copyright © 2016 Nivendru Gavaskar. All rights reserved.
//

#import "TrendingHaggleVC.h"
#import "AppDelegate.h"
#import "Trendinghagglecell1.h"
#import "Trendinghagglecell2.h"

@interface TrendingHaggleVC ()<UITableViewDelegate,UITableViewDataSource>

@end

@implementation TrendingHaggleVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)didTapback:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if(tableView.tag==1)
    {
        Trendinghagglecell1 *cell=(Trendinghagglecell1 *)[self.tblvw1 dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
        cell.myvw.layer.borderColor=[UIColor lightGrayColor].CGColor;
        cell.myvw.layer.borderWidth=1.0;
        cell.myvw.layer.cornerRadius=5.0;
        
        return cell;
    }
    Trendinghagglecell2 *cell=(Trendinghagglecell2 *)[self.tblvw2 dequeueReusableCellWithIdentifier:@"Cell1" forIndexPath:indexPath];
    cell.myvw1.layer.borderColor=[UIColor lightGrayColor].CGColor;
    cell.myvw1.layer.borderWidth=1.0;
    cell.myvw1.layer.cornerRadius=5.0;
    return cell;
    
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(tableView.tag==1)
    {
        return 3;
    }
    return 5;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

@end
