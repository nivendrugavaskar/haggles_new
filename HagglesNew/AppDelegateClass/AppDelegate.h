//
//  AppDelegate.h
//  HagglesNew
//
//  Created by Nivendru Gavaskar on 21/01/16.
//  Copyright © 2016 Nivendru Gavaskar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NSString+Validation.h"
#import "AFNetworking.h"
#import "UIKit+AFNetworking.h"
#import "SVProgressHUD.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate>
{
@public
    NSString *parentUrl;
    BOOL loginstatus,signupstatus;
}
@property (strong, nonatomic) UIWindow *window;
@end

