//
//  CustomtabBar.m
//  HagglesNew
//
//  Created by Nivendru Gavaskar on 21/01/16.
//  Copyright © 2016 Nivendru Gavaskar. All rights reserved.
//

#import "CustomtabBar.h"


@interface CustomtabBar ()
{
    
}

@end

@implementation CustomtabBar



- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    app=(AppDelegate *)[UIApplication sharedApplication].delegate;
    // self.tabbardelegate=(id)(CalculatorViewcontroller *)[self.viewControllers objectAtIndex:0];
    self.tabBar.hidden=YES;
    NSLog(@"tabbarheight=%f",self.tabBar.frame.size.height);
    //self.selectedIndex=0;
    _tabbarscroller=[[UIScrollView alloc] initWithFrame:CGRectMake(0, self.view.frame.size.height-self.tabBar.frame.size.height, self.tabBar.frame.size.width, self.tabBar.frame.size.height)];
    _tabbarscroller.backgroundColor=[UIColor whiteColor];
    [_tabbarscroller setContentSize:CGSizeMake(self.tabBar.frame.size.width, 0)];
    // _tabbarscroller.layer.borderColor=[UIColor redColor].CGColor;
    //_tabbarscroller.layer.borderWidth=1.0;
    _tabbarscroller.showsHorizontalScrollIndicator=NO;
    
    UILabel *lbl=[[UILabel alloc] initWithFrame:CGRectMake(0, 0, _tabbarscroller.frame.size.width, 1)];
    lbl.backgroundColor=[UIColor lightGrayColor];
    [_tabbarscroller addSubview:lbl];
    
    UIImageView *tabbarimage=[[UIImageView alloc] initWithFrame:_tabbarscroller.bounds];
    tabbarimage.tag=-1;
    tabbarimage.contentMode=UIViewContentModeScaleAspectFill;
    tabbarimage.clipsToBounds=YES;
    [_tabbarscroller addSubview:tabbarimage];
    [self addbuttons];
    [self.view addSubview:_tabbarscroller];
    // By defauult select 1
    [self activeModeTabSelect:0];
    
}
#pragma adding tabs in customize Tab bar
-(void)addbuttons
{
    for (int i=0; i<=3; i++)
    {
        UIButton *btn=[[UIButton alloc] initWithFrame:CGRectMake(0, 0, (self.view.frame.size.width/4),self.tabBar.frame.size.height)];
        btn.tag=i;
        
    
        btn.frame=CGRectMake(self.view.frame.size.width/4*i, 0, (self.view.frame.size.width/4), self.tabBar.frame.size.height);
    
       // btn.backgroundColor=[UIColor colorWithPatternImage:[UIImage imageNamed:@"tab_hover1.png"]];
        btn.backgroundColor=[UIColor clearColor];
        // btn.layer.borderWidth=1.0;
        // btn.layer.borderColor=[UIColor redColor].CGColor;
        [btn addTarget:self action:@selector(buttontapped:) forControlEvents:UIControlEventTouchUpInside];
        
        
        UIImageView *Img=[[UIImageView alloc] initWithFrame:CGRectMake(0,0,(self.view.frame.size.width/4), self.tabBar.frame.size.height)];
        Img.tag=i;
        Img.frame=CGRectMake(btn.frame.origin.x+5, btn.frame.origin.y+5, btn.frame.size.width-10, btn.frame.size.height-10);
        Img.contentMode=UIViewContentModeScaleAspectFit;
        Img.clipsToBounds=YES;
        Img.backgroundColor=[UIColor clearColor];
        switch (btn.tag)
        {
                
            case 0:
                Img.image=[UIImage imageNamed:@"home-2.png"];
                break;
            case 1:
                Img.image=[UIImage imageNamed:@"Notification-2.png"];
                break;
            case 2:
                Img.image=[UIImage imageNamed:@"MyHaggle-2.png"];
                break;
            case 3:
                Img.image=[UIImage imageNamed:@"Settings-2.png"];
                break;
            
            default:
                break;
        }
        
        [_tabbarscroller addSubview:btn];
        [_tabbarscroller addSubview:Img];
    }
    
}
#pragma selecting by default search TAb
-(void)activeModeTabSelect:(int)tag
{
    [self setSelectedViewController:[self.viewControllers objectAtIndex:tag]];
    for (UIButton *btn in _tabbarscroller.subviews)
    {
        if([btn isKindOfClass:[UIButton class]])
        {
            switch (tag)
            {
                case 0:
                    btn.backgroundColor=[UIColor clearColor];
                    break;
                case 1:
                    btn.backgroundColor=[UIColor clearColor];
                    break;
                case 2:
                    
                    btn.backgroundColor=[UIColor clearColor];
                    break;
                case 3:
                    
                    btn.backgroundColor=[UIColor clearColor];
                    break;
                    
                default:
                    break;
            }
        }
    }
    [self deselectOtherButton:tag];
}
#pragma tabclick heirachy maintained
-(void)buttontapped:(id)sender
{
    // return;
    
    UIButton *btn=(UIButton *)sender;
    
    for (UIView *v in _tabbarscroller.subviews)
    {
        if([v isKindOfClass:[UIImageView class]])
        {
            UIImageView *img = (UIImageView *)v;
            if (btn.tag != img.tag)
            {
                continue;
            }
            switch (btn.tag)
            {
                    
                case 0:
                    img.image=[UIImage imageNamed:@"home-2.png"];
                    break;
                case 1:
                    img.image=[UIImage imageNamed:@"Notification-2.png"];
                    break;
                case 2:
                   img.image=[UIImage imageNamed:@"MyHaggle-2.png"];
                    
                    break;
                case 3:
                    img.image=[UIImage imageNamed:@"Settings-2.png"];
                    
                    break;
               
                default:
                    break;
            }
        }
    }
    
    [self deselectOtherButton:(int)btn.tag];
    [self setSelectedViewController:[self.viewControllers objectAtIndex:btn.tag]];
    
    
}
#pragma deselecting tab once select other
-(void)deselectOtherButton:(int )tager
{
    
       
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES animated:NO];
    // [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault animated:NO];
    
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
