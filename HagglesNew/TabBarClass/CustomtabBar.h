//
//  CustomtabBar.h
//  HagglesNew
//
//  Created by Nivendru Gavaskar on 21/01/16.
//  Copyright © 2016 Nivendru Gavaskar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

@interface CustomtabBar : UITabBarController
{
    AppDelegate *app;
}

@property(strong,nonatomic)UIScrollView *tabbarscroller;

@end
