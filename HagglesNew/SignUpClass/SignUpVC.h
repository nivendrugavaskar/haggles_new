//
//  SignUpVC.h
//  HagglesNew
//
//  Created by Nivendru Gavaskar on 21/01/16.
//  Copyright © 2016 Nivendru Gavaskar. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SignUpVC : UIViewController
@property (weak, nonatomic) IBOutlet UITextField *txtname;
@property (weak, nonatomic) IBOutlet UITextField *txtpwd;
@property (weak, nonatomic) IBOutlet UITextField *txtemail;
- (IBAction)didTapSignup:(id)sender;

@end
